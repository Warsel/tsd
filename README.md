# Лабораторные работы по предмету ТРПО #

Темой в данных лабораторных работах было выбрано предприятие "Сайт для бронирования билетов в театре на спектакль".

## Лабораторная работа №1 ##

### Определить основные процессы работы любого предприятия: определить бизнес-процессы и промоделировать их на языке IDEF0, дополнить IDEF3 & DFD.  ###

#### Выполненное задание: ####

Определим обобщённую модель предприятия на языке IDEF0:

![](/images/IDEF0.jpg)

Построим более подробную модель IDEF0:

![](/images/IDEF0_ALL.jpg)

Определим процесс обработки запроса клиента на бронирование билета в театр на спектакль с помощью методологии IDEF3:

![](/images/IDEF3.jpg)

Определим автомацизацию бронирования билетов с помощью методологии DFD (без композиции):

![](/images/DFD.jpg)

Проведём декомпозицию автомацизации (DFD):

![](/images/DFD_DECOMPOSITION.jpg)

В итоге лабораторная работа выполнена. Результат - определение процесса работы "Сайта для бронирования билетов в театре на спектакль".

## Лабораторная работа №2 ##

### Разработать требования к изделию. ###

#### Выполненное задание: ####

##### 1. Общие сведения #####

Данный документ является техническим заданием на разработку Сайта для бронирования билетов в театре на спектакль.

##### 2. Язык #####

Немецкий, Английский (многоязычный сайт).

##### 3. Структура сайта для бронирования билетов в театре #####

Структура Сайта для бронирования билетов в театре предусматривает введение следующих страниц:

1. Регистрация/Вход;
2. Выбор спектакля;
3. Выбор места в зале;
4. Пользовательское соглашение об использовании сайта;
5. Пользовательское соглашение о бронировании билета;
6. Контакты.

##### 4. Особенности функционала #####

1. Перед регистрацией пользователь должен дать своё согласие на правила использования сайта после прочтения Пользовательского соглашения об использовании сайта. При отказе, регистрация прекращается.
2. Регистрация на сайте происходит с помощью аутентификации по телефону. Пользователю небходимо ввести своё имя, адрес электронной почты и личный номер телефона для регистрации. После этого он получает смс-код для завершения регистрации.
3. Вход в систему также осуществляется с помощью смс-кода. Пользователь вводит личный номер телефона и ему приходит смс-код.
4. Перед бронированием билета пользователь должен дать своё согласие на правила бронирования билета после прочтения Пользовательского соглашения о бронировании билета. При отказе, бронирование прекращается.
5. После успешного бронирования пользователю должно прийти письмо с подробной информацией о бронировании билета на указанную при регистрации почту.
6. После отмены бронировния пользователю должно прийти письмо об успешной отмены забронированного билета на указанную при регистрации почту.
7. На странице Контакты должны быть: номера телефонов Службы поддержки.


##### 5. Технические характеристики программных средств и среды функционирования #####

Сайт будет разработан с помощью ASP.NET Core 3. В качестве БД будет использоваться Firestore от Firebase.

## Лабораторная работа №3 ##

### Описать в табличном и текстовом специальном виде USE CASE для реализации требований. Начальная формализация.  ###

#### Выполненное задание: ####

| ID | Имя use case | Действующие лица | Описание | Предусловие | Основной сценарий | Альтернативный сценарий | Результат |
| -- | ------------ | ---------------- | -------- | ----------- | ----------------- | ----------------------- | --------- |
| 1 | Регистрация пользователя в системе | Пользователь. Система | Пользователь регистрируется на сайте для дальнейшего бронирования билетов | Пользователь не зарегистрирован в системе | 1. Пользователь заходит на страницу регистации <br/>2. Пользователь соглашается с Пользовательским соглашением об использовании сайта <br/>3. Пользователь вводит необходимую информацию и она отправляется в систему <br/>4. Пользователь вводит пришедшый на телефон смс-код <br/>5. Авторизованного пользователя переадресовывает на главную страницу сайта | 3. Пользователь не соглашается и его переадресовывает на главную страницу сайта | В системе создаётся новый пользователь. Пользователь авторизован на сайте |
| 2 | Авторизация пользователя в системе | Пользователь. Система | Пользователь авторизируется на сайте для дальнейшего бронирования билетов | Пользователь зарегистрирован в системе | 1. Пользователь заходит на страницу авторизации. <br/>2. Пользователь вводит Логин и Пароль <br/>3. Пользователь вводит пришедший на телефон смс-код <br/>4. Авторизованного пользователя переадресовывает на главную страницу сайта | | Пользователь авторизован на сайте |
| 3 | Бронирование билета пользователем | Пользователь. Система | Пользователь бронирует билет на спектакль | Пользователь авторизован | 1. Пользователь на главном сайте переходит на страницу бронирования <br/>2. Пользователь выбирает доступный спектакль (дату и время) <br/>3. Пользователь выбирает свободное место в зале. <br/>4. Пользователь соглашается с Пользовательским соглашением о бронировании билета <br/>5. Пользователю на электронную почту приходит письмо с подробной информацией о бронировании билета | 5. Пользователь не соглашается и его переадресовывает на главную страницу сайта | Выбранное место на спектакль забронировано пользователем |
| 4 | Отмена бронирования билета пользователем | Пользователь. Система | Пользователь отменяет бронь билета на спектакль | Пользователь авторизован. У пользователя есть забронированный билет. Время до показа спектакля - не меньше 3-х дней | 1. Пользователь на главном сайте переходит в личный кабинет. <br/>2. Пользователь выбирает забронированный билет, который хочет отменить <br/>3. Пользователь нажимает кнопку "Отменить" <br/>4. Пользователя переадресовывает на страницу личного кабинета 5. Пользователю на электронную почту приходит письмо с уведомлением об отмене бронирования билета | | У пользователя нет отменённого билета. Билет становится свободным (незабронированным) |
| 5 | Восстановление аккаунта пользователя (смена номера телефона) | Пользователь. Администратор. Система | У пользователя изменился номер телефона и он не может авторизироваться на сайте | Пользователь зарегистрирован в системе. У пользователя изменился номер теефона | 1. Пользователь заходит на страницу авторизации <br/>2. Пользователь нажмает кнопку "Восстановить аккаунт" <br/>3. Пользователь указывает причину "Изменился номер телефона" <br/>4. Пользователю на электронную почту приходит письмо для подтверждения сброса номера телефона <br/>5. При нажатии кнопки "Потвердить" пользователя переадресовывает на страницу изменения номера телефона. <br/>6. Пользователь вводит необходимую информацию и нажимает кнопку "Изменить". <br/>7. Пользователь вводит пришедший на новый телефон смс-код |  | У пользователя изменился номер телефона |

## Лабораторная работа №4.1 ##

### На языке UML спроектировать ПО: ###
* промоделировать реализацию use case с помощью S.D.;
* перейти и выполнить моделирование с помощью диаграмм классов получить код для реализации (основа - см. диаграммы выше);
* построить диарамму компонент из диаграмм классов;

#### Выполненное задание: ####

Sequence Diagram

Use case: Регистрация пользователя в системе

![](/images/Sequence_Diagram_Sign_Up.jpg)

Use case: Авторизация пользователя в системе

![](/images/Sequence_Diagram_Sign_In.jpg)

Use case: Бронирование билета пользователем

![](/images/Sequence_Diagram_Booking.jpg)

Use case: Отмена бронирования билета пользователем

![](/images/Sequence_Diagram_Booking_Cancellation.jpg)

Static Structure Diagram

![](/images/Class_Diagram.png)

Component Diagram

![](/images/Component_Diagram.png)
    
## Лабораторная работа №4.2 ##

### На языке IDEF1X спроектировать БД, сгенировать DDL, построить БД, которая будет применяться в п. 4.1. ###

#### Выполненное задание: ####

IDEF1X

![](/images/IDEF1X.png)

DDL

Users

``` SQL

CREATE TABLE [dbo].[Users] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (MAX) NULL,
    [Email]       NVARCHAR (MAX) NULL,
    [PhoneNumber] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([Id] ASC)
);

```

Tickets

``` SQL

CREATE TABLE [dbo].[Tickets] (
    [Id]            INT IDENTITY (1, 1) NOT NULL,
    [SeatNumber]    INT NOT NULL,
    [UserId]        INT NULL,
    [PerformanceId] INT NULL,
    [TheatreId]     INT NULL,
    CONSTRAINT [PK_Tickets] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Tickets_Performance_PerformanceId] FOREIGN KEY ([PerformanceId]) REFERENCES [dbo].[Performance] ([Id]),
    CONSTRAINT [FK_Tickets_Theatres_TheatreId] FOREIGN KEY ([TheatreId]) REFERENCES [dbo].[Theatres] ([Id]),
    CONSTRAINT [FK_Tickets_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);

```

Performance

``` SQL

CREATE TABLE [dbo].[Performance] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (MAX) NULL,
    [Director]  NVARCHAR (MAX) NULL,
    [Actors]    NVARCHAR (MAX) NULL,
    [TheatreId] INT            NULL,
    CONSTRAINT [PK_Performance] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Performance_Theatres_TheatreId] FOREIGN KEY ([TheatreId]) REFERENCES [dbo].[Theatres] ([Id])
);

```

Theatres

``` SQL

CREATE TABLE [dbo].[Theatres] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [Name]    NVARCHAR (MAX) NULL,
    [Address] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Theatres] PRIMARY KEY CLUSTERED ([Id] ASC)
);

```

## Лабораторная работа №5 ##

### Выполнить тестирование по любому критерию  ###

#### Выполненное задание: ####

Tests

```
namespace TSD.Tests
{
    public class TSDTests
    {
        private readonly Database _mockDatabase = null;

        public TSDTests(): base()
        {
            _mockDatabase = new Database();
        }

        [Fact]
        public void RegisterNewUser()
        {
            var user = new User
            {
                Name = "Test_Name",
                Email = "Test_Email",
                PhoneNumber = "Test_Phone_Number"
            };

            var result = _mockDatabase.RegisterUser(user);

            Assert.True(result);
        }

        [Fact]
        public void RegisterInvalidUser()
        {
            User user = null;

            var result = _mockDatabase.RegisterUser(user);

            Assert.False(result);
        }

        [Fact]
        public void AuthUser()
        {
            var email = "Test_Email";
            var password = "1234567890";

            var result = _mockDatabase.AuthUser(email, password);

            Assert.True(result);
        }

        [Fact]
        public void TryAuthUserWithWrongCredential()
        {
            var email = "Test_Email1";
            var password = "1234567890";

            var result = _mockDatabase.AuthUser(email, password);

            Assert.False(result);
        }

        [Fact]
        public void BookTicketByUser()
        {
            var userId = 1;
            var ticketId = 1;

            var result = _mockDatabase.BookTicket(userId, ticketId);

            Assert.True(result);
        }

        [Fact]
        public void BookNonExistentTicket()
        {
            var userId = 1;
            var ticketId = 2;

            var result = _mockDatabase.BookTicket(userId, ticketId);

            Assert.False(result);
        }

        [Fact]
        public void BookTicketWithNonExistentUser()
        {
            var userId = 2;
            var ticketId = 1;

            var result = _mockDatabase.BookTicket(userId, ticketId);

            Assert.False(result);
        }

        [Fact]
        public void CancelBookingOfTicket()
        {
            var userId = 1;
            var ticketId = 1;

            var result = _mockDatabase.BookTicket(userId, ticketId);

            Assert.True(result);
        }

        [Fact]
        public void CancelBookingOfNonExistentTicket()
        {
            var userId = 1;
            var ticketId = 2;

            var result = _mockDatabase.BookTicket(userId, ticketId);

            Assert.False(result);
        }

        [Fact]
        public void CancelBookingOfTicketWithNonExistentUser()
        {
            var userId = 2;
            var ticketId = 1;

            var result = _mockDatabase.BookTicket(userId, ticketId);

            Assert.False(result);
        }

        [Fact]
        public void ChangePhoneNumber()
        {
            var userId = 1;
            var phoneNumber = "0987654321";

            var result = _mockDatabase.ChangeUserPhoneNumber(userId, phoneNumber);

            Assert.True(result);
        }

        [Fact]
        public void ChangePhoneNumberWithInvalidValue()
        {
            var userId = 1;
            var phoneNumber = "";

            var result = _mockDatabase.ChangeUserPhoneNumber(userId, phoneNumber);

            Assert.False(result);
        }

        [Fact]
        public void ChangePhoneNumberWithNonExistentUser()
        {
            var userId = 2;
            var phoneNumber = "0987654321";

            var result = _mockDatabase.ChangeUserPhoneNumber(userId, phoneNumber);

            Assert.False(result);
        }
    }
}

```

Tests Results

![](/images/Test_Results.png)
